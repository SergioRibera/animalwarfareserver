module.exports = class Position{
    constructor(idPlayer){ 
        this.myPlayer = idPlayer;
        this.x = 0.0;
        this.y = 0.0;
        this.z = 0.0;
        this.rx = 0.0;
        this.ry = 0.0;
        this.rz = 0.0;
        this.jx = 0.0;
        this.jy = 0.0;
    }
    serialize(){
        return JSON.stringify(this);
    }
    static fromJson(json){
        return JSON.parse(json);
    }
}
