
module.exports = class Notify{
    constructor(_title, _content, _type){
        this.title = _title;
        this.content = _content;
        this.type = _type;
    }
    /*
     *  0 = Info
     *  1 = Warning
     *  2 = Error
     */ 
    static NotRoomIDValid(){
        return new Notify('Error on Join Room', 'The ID not is valid :c', 2);
    }
    static NotRoomTypeValid(){
        return new Notify('Error on Join Room', 'The type room is invalid :c', 2);
    }
    static NotRoomIsFull(){
        return new Notify('Error on join room', 'The room is full :c', 2);
    }
}
