const Room = require('./Room');
const random = require('./Helper').rnd;
const shortid = require('shortid');
const Notify = require('./Notify');

module.exports = class Server{
    constructor(sets) {
        this.roomsFlagMode = [];
        this.roomsTankMode = [];
        this.amountPlayers = 0;
        this.amountPlayersFlagMode = 0;
        this.amountPlayersTankMode = 0;
        this.settings = sets;
    }
    changeSettings(sets){
        this.settings = sets;
    }
    onConnected(socket){
        this.amountPlayers++;
        console.log(`New player connection`, `Total players: ${this.amountPlayers}`, `Total Rooms Flag Mode: ${this.amountPlayersFlagMode}`,`Total Rooms Tank Mode: ${this.amountPlayersTankMode}`);
        
        socket.emit("joined-server", shortid.generate());
        socket.on("join-room", (data) => {
            if(!data.idRoom || data.idRoom === "" || data.idRoom.length === 0){
                this.createRoom(data, socket);
            } else if(this.roomsFlagMode.includes(data.idRoom) || this.roomsTankMode.includes(data.idRoom)){
                this.getRoomByType(data.roomType, data.idRoom).addPlayer(data, socket);
                /*if(data.roomType == 0){
                    this.roomsFlagMode[data.idRoom].addPlayer(data, socket);
                    this.amountPlayersFlagMode++;
                }else if(data.roomType == 1){
                    this.roomsTankMode[data.idRoom].addPlayer(data, socket);
                    this.amountPlayersTankMode++;
                }else
                    socket.emit("notify", Notify.NotRoomTypeValid());*/
            }else if (data.roomType != -1){
                this.getRoomByType(data.roomType, this.getRandomRoom(data.roomType)).addPlayer(data, socket);
            } else
                socket.emit("notify", Notify.NotRoomIDValid());
            console.log(`Total Rooms Flag Mode: ${this.amountPlayersFlagMode}`, `Total Rooms Tank Mode: ${this.amountPlayersTankMode}`);
        });
    }
    createRoom(data, socket){
        let room = new Room(this);
        switch(data.roomType){
            case 0:
                this.roomsFlagMode[room.id] = room;
                data.idRoom = room.id;
                this.roomsFlagMode[room.id].addPlayer(data, socket);
                this.amountPlayersFlagMode++;
                break;
            case 1:
                this.roomsTankMode[room.id] = room;
                data.idRoom = room.id;
                this.roomsTankMode[room.id].addPlayer(data, socket);
                this.amountPlayersTankMode++;
                break;
            default:
                socket.emit("notify", Notify.NotRoomTypeValid());
                break;
        }
    }
    getRoomByType(type, id){
        if(type == 0)
            return this.roomsFlagMode[id];
        else if(type == 1)
            return this.roomsTankMode[id];
        else
            console.error(`Room Type is not valid ${type}`);
    }
    getRandomRoom(roomType){
        let idRoom = null,
            value = random(0, roomType == 0 ? Object.keys(this.roomsFlagMode).length : Object.keys(this.roomsTankMode).length);
        let roomsAviables = roomType == 0 ? this.roomsFlagMode.filter(r => (r.getPlayersCount() + 1) <= this.settings.flagMode.maxPlayers) : this.roomsTankMode.filter(r => (r.getPlayersCount() + 1) <= this.settings.tankMode.maxPlayers),
            sizeRoomsAviable = Object.keys(roomsAviables).length;
        if(sizeRoomsAviable > 0){
            value = random(0, sizeRoomsAviable);
            idRoom = Object.keys(roomsAviables) [value];
        }
        return idRoom;
    }
    deleteRoom(type, id){
        if(type == 0){
            this.amountPlayersFlagMode = this.calculatePlayers(this.roomsFlagMode);
            delete this.roomsFlagMode[id];
        } else if(type == 1){
            this.amountPlayersTankMode = this.calculatePlayers(this.roomsTankMode);
            delete this.roomsTankMode[id];
        }
        console.log(`Total Players Flag Mode: ${this.amountPlayersFlagMode}`,
            `Total Players Tank Mode: ${this.amountPlayersTankMode}`,
            `Total Rooms Flag Mode: ${Object.keys(this.roomsFlagMode).length}`,
            `Total Rooms Tank Mode: ${Object.keys(this.roomsTankMode).length}`);
    }
    calculatePlayers(rooms){
        let players = 0;
        if(Object.keys(rooms).length > 0){
            rooms.forEach(room => {
                players += Object.keys(room.players).length;
            });
        }
        return players;
    }
}
