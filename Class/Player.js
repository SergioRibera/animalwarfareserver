let shortID = require('shortid');
const Position = require('./Position');

module.exports = class Player{
    constructor(){
        this.username = "";
        this.idRoom = "";
        this.idPos = -1;
        this.roomType = -1;
        this.id = shortID.generate();
        this.skin = 0, hatSkin = 0, weaponSkin = 0, bulletSkin = 0;
        this.pos = new Position(this.id);
    }
    serialize(){
        return JSON.stringify(this);
    }
    static fromJson(json){
        return JSON.parse(json);
    }
}
