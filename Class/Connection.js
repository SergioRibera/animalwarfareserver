module.exports = class Connection{
    constructor(player, socket){
        this.player = player;
        this.socket = socket;
    }
    sendData(name, data){
        this.socket.emit(name, data);
    }
}
