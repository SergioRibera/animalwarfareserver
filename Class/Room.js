const shortID = require('shortid');
const random = require('./Helper').rnd;
const Connection = require('./Connection');
const Notify = require('./Notify');
module.exports = class Room{
    constructor(server){
        this.players = [];
        this.type = -1;
        this.id = shortID.generate(); 
        this.isStartPlay = false;
        this.server = server;
        this.time = server.settings.flagMode.timeRound;
    }
    addPlayer(player, socket){
        if(Object.keys(this.players).length + 1 > this.server.settings.flagMode.maxPlayers){
            socket.emit("notify", Notify.NotRoomIsFull());
            socket.emit("joined-room", "notentry");
            return;
        }
        player.idRoom = this.id;
        player.idPos = this.rnd(0, this.server.settings.flagMode.maxPositionsInits);
        this.type = player.roomType;
        this.players[player.id] = new Connection(player, socket);
        //socket.on("ready-to-play", data => this.sendData("ready-to-play", data));
        socket.on("move-player", data => this.sendData("move-player",data));
        socket.on("anim-player", data => this.sendData("anim-player", data));
        socket.on("shoot-server", data => this.sendData("shoot", data));
        socket.on('disconnect', () => this.disconnectPlayer(player));
        socket.emit("joined-room", player);
        if(this.amountPlayers == this.server.settings.flagMode.maxPlayers)
            this.startGame();
        setTimeout(() => this.startGame(), /*this.server.timeWaitAllPlayers*/30000);
    }
    startGame(){
        if(!this.isStartPlay){
            this.sendData("start-game", Object.keys(this.players).length);
            this.isStartPlay = true;
        }
        let counter = setInterval(() => {
            this.time--;
            this.sendData("game-time-change", this.time);
            if(this.time <= 0)
                clearInterval(counter);
        }, 1000);
    }
    disconnectPlayer(player){
        this.sendData("left-room", player.id);
        this.server.amountPlayers--;
        delete this.players[player.id];
        if(Object.keys(this.players).length <= 0)
            this.server.deleteRoom(this.type, this.id);
        console.log("Players on room: " + Object.keys(this.players).length)
        console.log("Disconnect Player " + player.id);
    }
    sendData(name, data){
        for(var key in this.players)
            this.players[key].sendData(name, data);
        //console.log(name + " transmited");
    }
    getPlayersCount(){
        return Object.keys(this.players).length;
    }
    rnd(min, max){
        if(this.players.keys().length < 2)
            return random(min, max);
        let isValid = false;
        let value = random(min, max);
        while(!isValid){
            for(var key in this.players){
                if(this.players[key].player.idPos != value){
                    isValid = true;
                    break;
                }else{
                    isValid = false;
                    value = this.random(min, max);
                }
            }
        }
        return value;
    }
}
