const Server = require('./Class/Server');

let app = require('express')();
let http = require('http').createServer(app);
let io = require('socket.io')({
    transports: ['websocket']
});

io.attach(4567);

["log", "warn", "error"].forEach((method) => {
    var oldMethod = console[method].bind(console);
    console[method] = function() {
        let date = new Date();
        let argsv = Array.prototype.slice.call(arguments);
        let er ='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
        let wr ='<><><><><><><><><><><><><><><><><><><><>';
        let ir ='----------------------------------------';
        let id = method=="log"?ir:(method == "warn" ? wr : er);
        let txt = `${id} ${date.toUTCString()} ${id}\n ${argsv.join('\n')}`;
        oldMethod.apply(console, [txt]);
        // Save in file
    };
});

let settings = {
    flagMode: {
        maxPlayers: 10,
        timeWaitAllPlayers: 60000,
        timeRound: 360,
        maxPositionsInits: 10
    },
    tankMode: {
        maxPlayers: 15,
        timeWaitAllPlayers: 60000
    }
};
let server = new Server(settings);
io.on('connection', (socket) => {
    server.onConnected(socket);
});

/*http.listen(80, () => {
    console.log("Server Listener *:4000");
})*/
